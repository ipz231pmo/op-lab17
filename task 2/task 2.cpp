﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
int quadricEquation(double a, double b, double c, double x1, double x2) {
    double d = b * b - 4 * a * c;
    if (d < 0) return 0;    
    double fx1 = ( - b + sqrt(d) ) / (2 * a), fx2 = (-b-sqrt(d)) / (2 * a);
    if (d == 0 && fabs(fx1 - x1) < 0.01) return 1;
    else if (d==0)return -1;

    if (fabs(fx1-x1)<0.01 && fabs(fx2 - x2)<0.01) return 2;
    return -1;
}
int main() {
    double a, b, c, x1, x2;
    printf("Enter coefs and roots of quadratic equation: ");
    scanf("%lf %lf %lf %lf %lf", &a, &b, &c, &x1, &x2);
    printf("There is %d roots\n", quadricEquation(a, b, c, x1, x2));
    return 0;
}