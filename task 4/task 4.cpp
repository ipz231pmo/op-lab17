﻿#include <stdio.h>
#include <Windows.h>
#include <math.h>

double Leng(double, double, double, double);
double Perim(double, double, double, double, double, double);
double Area(double, double, double, double, double, double);
double Dist(double, double, double, double, double, double);
void Altitudes(double, double, double, double, double, double, double*, double*, double*);

int main(){
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    double ax, ay, bx, by, cx, cy, dx, dy, px, py;
    printf("Введіть координати точок:");
    printf("\n\tТочка A: ");
    scanf_s("%lf %lf", &ax, &ay);
    printf("\tТочка B: ");
    scanf_s("%lf %lf", &bx, &by);
    printf("\tТочка C: ");
    scanf_s("%lf %lf", &cx, &cy);
    printf("\tТочка D: ");
    scanf_s("%lf %lf", &dx, &dy);
    printf("\tТочка P: ");
    scanf_s("%lf %lf", &px, &py);
    // a (Lengh)
    printf("Довжини відрізків:\n\tAB = %lf\n\tAC = %lf\n\tAD = %lf\n", Leng(ax, ay, bx, by), Leng(ax, ay, cx, cy), Leng(ax, ay, dx, dy));
    // b (Perim)
    printf("Периметри трикутників:\n\tABC = %lf\n\tABD = %lf\n\tACD = %lf\n", Perim(ax, ay, bx, by, cx, cy), Perim(ax, ay, bx, by, dx, dy), Perim(ax, ay, cx, cy, dx, dy));
    // c (Area)
    printf("Площі трикутників:\n\tABC = %lf\n\tABD = %lf\n\tACD = %lf\n", Area(ax, ay, bx, by, cx, cy), Area(ax, ay, bx, by, dx, dy), Area(ax, ay, cx, cy, dx, dy));
    // d (Dist)
    printf("Відстань від точки P до прямих:\n\tAB = %lf\n\tAC = %lf\n\tBC = %lf\n", Dist(px, py, ax, ay, bx, by), Dist(px, py, ax, ay, cx, cy), Dist(px, py, bx, by, cx, cy));
    // e (Altitudes)
    double h1, h2, h3;
    // ABC
    Altitudes(ax, ay, bx, by, cx, cy, &h1, &h2, &h3);
    printf("Висоти трикутника ABC:\n");
    printf("\tВисота, проведена з вершини A = %lf\n", h1);
    printf("\tВисота, проведена з вершини B = %lf\n", h2);
    printf("\tВисота, проведена з вершини C = %lf\n", h3);
    // ABD
    Altitudes(ax, ay, bx, by, dx, dy, &h1, &h2, &h3);
    printf("Висоти трикутника ABD:\n");
    printf("\tВисота, проведена з вершини A = %lf\n", h1);
    printf("\tВисота, проведена з вершини B = %lf\n", h2);
    printf("\tВисота, проведена з вершини D = %lf\n", h3);
    // ACD
    Altitudes(ax, ay, cx, cy, dx, dy, &h1, &h2, &h3);
    printf("Висоти трикутника ACD:\n");
    printf("\tВисота, проведена з вершини A = %lf\n", h1);
    printf("\tВисота, проведена з вершини C = %lf\n", h2);
    printf("\tВисота, проведена з вершини D = %lf\n", h3);
    return 0;    
}

double Leng(double xa, double ya, double xb, double yb){
    return pow(pow(xa-xb, 2) + pow(ya-yb, 2), 1/2.);
}
double Perim(double xa, double ya, double xb, double yb, double xc, double yc){
    return Leng(xa, ya, xb, yb) + Leng(xb, yb, xc, yc) + Leng(xa, ya, xc, yc);    
}
double Area(double x1, double x2, double y1, double y2, double z1, double z2){
    double p = Perim(x1, x2, y1, y2, z1, z2) / 2.;
    return pow(p * (p - Leng(x1, x2, y1, y2)) * (p - Leng(x1, x2, z1, z2)) * (p - Leng(y1, y2, z1, z2)), 1 / 2.);
}
double Dist(double x1, double x2, double y1, double y2, double z1, double z2){
    return 2 * Area(x1, x2, y1, y2, z1, z2) / Leng(y1, y2, z1, z2);
}
void Altitudes(double xa, double ya, double xb, double yb, double xc, double yc, double *ha, double *hb, double *hc){
    *ha = Dist(xa, ya, xb, yb, xc, yc);
    *hb = Dist(xb, yb, xa, ya, xc, yc);
    *hc = Dist(xc, yc, xa, ya, xb, yb);
}