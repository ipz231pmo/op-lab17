﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
double TriangleP(double a, double h) {
    if (a <= 0 || h <= 0) throw "Bad Sides";
    return a + 2 * sqrt(a*a/4 + h*h);
}
int main() {
    for (int i = 0; i < 3; i++) {
        double a, h;
        printf("Enter base of triangle and its height: ");
        scanf("%lf %lf", &a, &h);
        printf("Perimetr of this triangle is %f\n", TriangleP(a, h));
    }
    return 0;
}