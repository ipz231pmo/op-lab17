﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
double percent(double num, double perc) {
    return num / 100. * perc;
}
int main(){
    double a, b;
    printf("Enter number and percent, that you want know: ");
    scanf("%lf %lf", &a, &b);
    printf("%f percent of %f is %f", b, a, percent(a, b));
    return 0;
}